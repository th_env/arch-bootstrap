# Bootstrap Scripts

This repository contains setup scripts for the various architecturse I use or have used.

This includes:
- OSX
- Linux (Ubuntu)

| Architecture | Tag |
|--------------|-----|
| MacOS | macos |

## Usage

This project contains a Makefile that exposes a single command to setup a machine, based on the tools that I use, which a level of configurability in the things that I need to be configurable (e.g kubectl etc).

To run the default configuration, execute the following command from the command line:

```bash
make bootstrap
```

## Prerequisites

To run this project, you must have ansible installed and available on your current PATH. The best way of doing this is probably via Python's pip package manager. Ansible's own documentation provides an up to date means of [installing via pip](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-and-upgrading-ansible-with-pip).

### System Specific Prerequisites

#### MacOS

On MacOS, the playbook will require [homebrew](https://brew.sh) to be installed.
