## internal makefile use only

requirements:
	ansible-galaxy install -g -f -r roles/requirements.yml

## user facing commands

lint:
	ansible-playbook --syntax-check main.yml

ifdef TAGS
bootstrap:
	ansible-playbook --check main.yml --tags $$TAGS
else
bootstrap:
	ansible-playbook --check main.yml
endif

.PHONY: bootstrap requirements
